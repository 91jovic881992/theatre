-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: theatre
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actor` (
  `id_actor` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_actor`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
INSERT INTO `actor` VALUES (1,'Ethan','Hawke'),(2,'Kyle','MacLachlan'),(3,'Diane','Venora'),(4,'Sam','Shepard'),(5,'Bill','Murray'),(6,'Liev','Schreiber'),(7,'Julia','Stiles'),(8,'Steve','Zahn'),(9,'Julie','Andrews'),(10,'Dick','Van Dyke'),(11,'Davdi','Tomlinson'),(12,'Gylnis','Johns'),(13,'Hermione','Baddeley'),(14,'Reta','Shaw'),(15,'Karen','Dotrice'),(16,'Matthew','Gaber'),(17,'Gregory','Peck'),(18,'John','Megna'),(19,'Frank','Overton'),(20,'Rosemary','Murphy'),(21,'Ruth','White'),(22,'Brock','Peters'),(23,'Estelle','Evans'),(24,'Paul','Fix'),(25,'Denzel','Washington'),(26,'Frances','McDormand'),(27,'Alex','Hassell'),(28,'Bertie','Carvel'),(29,'Brendan','Gleeson'),(30,'Corey','Hawkins'),(31,'Harry','Melling'),(32,'Miles','Anderson'),(33,'Glenn','Ford'),(34,'Van','Heflin'),(35,'Richard','Jaeckel'),(36,'Russell','Crowe'),(37,'Chritian','Bale'),(38,'Ben','Foster');
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acts`
--

DROP TABLE IF EXISTS `acts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acts` (
  `id_acts` int(11) NOT NULL AUTO_INCREMENT,
  `id_actor` int(11) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_acts`),
  KEY `FK7ly4cnm8a2hbvypuwhjvy61rs` (`id_actor`),
  KEY `FK1tou8xju0rsaas9yn03lwtpl2` (`id_role`),
  CONSTRAINT `FK1tou8xju0rsaas9yn03lwtpl2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  CONSTRAINT `FK7ly4cnm8a2hbvypuwhjvy61rs` FOREIGN KEY (`id_actor`) REFERENCES `actor` (`id_actor`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acts`
--

LOCK TABLES `acts` WRITE;
/*!40000 ALTER TABLE `acts` DISABLE KEYS */;
INSERT INTO `acts` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5),(6,6,6),(7,7,7),(8,8,8),(9,9,9),(10,10,10),(11,11,11),(12,12,12),(13,13,13),(14,14,14),(15,15,15),(16,16,16),(17,17,17),(18,18,18),(19,19,19),(20,20,20),(21,21,21),(22,22,22),(23,23,23),(24,24,24),(25,25,25),(26,26,26),(27,27,27),(28,28,28),(29,29,29),(30,30,30),(31,31,31),(32,32,32),(33,33,33),(34,34,34),(35,35,35),(36,36,33),(37,37,34),(38,38,35);
/*!40000 ALTER TABLE `acts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acts_in_performance`
--

DROP TABLE IF EXISTS `acts_in_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acts_in_performance` (
  `id_acts_in_performance` int(11) NOT NULL AUTO_INCREMENT,
  `id_acts` int(11) DEFAULT NULL,
  `id_performance` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_acts_in_performance`),
  KEY `FKruy7i2bwo6oqq8p29naaaunit` (`id_acts`),
  KEY `FKs28nbha1vbrrlqplnfu62uqgs` (`id_performance`),
  CONSTRAINT `FKruy7i2bwo6oqq8p29naaaunit` FOREIGN KEY (`id_acts`) REFERENCES `acts` (`id_acts`),
  CONSTRAINT `FKs28nbha1vbrrlqplnfu62uqgs` FOREIGN KEY (`id_performance`) REFERENCES `performance` (`id_performance`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acts_in_performance`
--

LOCK TABLES `acts_in_performance` WRITE;
/*!40000 ALTER TABLE `acts_in_performance` DISABLE KEYS */;
INSERT INTO `acts_in_performance` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,1),(9,1,6),(10,2,6),(11,3,6),(12,4,6),(13,5,6),(14,6,6),(15,7,6),(16,8,6),(17,9,2),(18,10,2),(19,11,2),(20,12,2),(21,13,2),(22,14,2),(23,15,2),(24,16,2),(25,9,7),(26,10,7),(27,11,7),(28,12,7),(29,13,7),(30,14,7),(31,15,7),(32,16,7),(33,17,3),(34,18,3),(35,19,3),(36,20,3),(37,21,3),(38,22,3),(39,23,3),(40,24,3),(41,25,4),(42,26,4),(43,27,4),(44,28,4),(45,29,4),(46,30,4),(47,31,4),(48,32,4),(49,25,8),(50,26,8),(51,27,8),(52,28,8),(53,29,8),(54,30,8),(55,31,8),(56,32,8),(57,33,5),(58,34,5),(59,35,5),(60,36,9),(61,37,9),(62,38,9);
/*!40000 ALTER TABLE `acts_in_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `director` (
  `id_director` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_director`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'Michael','Almereyda'),(2,'Robert','Stevenson'),(3,'Robert','Mulligan'),(4,'Joel','Coen'),(5,'Delmer','Davis');
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favourites` (
  `id_favourites` int(11) NOT NULL AUTO_INCREMENT,
  `id_show` int(11) DEFAULT NULL,
  `id_visitor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_favourites`),
  KEY `FKp6yxgiunyrkrhydmg0ckqcvf4` (`id_show`),
  KEY `FK3rbkus94qipn2ij6uyippkrog` (`id_visitor`),
  CONSTRAINT `FK3rbkus94qipn2ij6uyippkrog` FOREIGN KEY (`id_visitor`) REFERENCES `visitor` (`id_visitor`),
  CONSTRAINT `FKp6yxgiunyrkrhydmg0ckqcvf4` FOREIGN KEY (`id_show`) REFERENCES `show_theatre` (`id_show`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favourites`
--

LOCK TABLES `favourites` WRITE;
/*!40000 ALTER TABLE `favourites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favourites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre` (
  `id_genre` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Action'),(2,'Crime'),(3,'Drama'),(4,'Thriller'),(5,'Western'),(6,'History'),(7,'Comedy'),(8,'Family'),(9,'Fantasy'),(10,'Romance');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre_show`
--

DROP TABLE IF EXISTS `genre_show`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre_show` (
  `id_genre_show` int(11) NOT NULL AUTO_INCREMENT,
  `id_genre` int(11) DEFAULT NULL,
  `id_show` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_genre_show`),
  KEY `FKf31894kvpgos8hx8hvdp5ttmj` (`id_genre`),
  KEY `FK54dtsd7nf43s3urevc1x0vsu7` (`id_show`),
  CONSTRAINT `FK54dtsd7nf43s3urevc1x0vsu7` FOREIGN KEY (`id_show`) REFERENCES `show_theatre` (`id_show`),
  CONSTRAINT `FKf31894kvpgos8hx8hvdp5ttmj` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre_show`
--

LOCK TABLES `genre_show` WRITE;
/*!40000 ALTER TABLE `genre_show` DISABLE KEYS */;
INSERT INTO `genre_show` VALUES (1,3,1),(2,10,1),(3,4,1),(4,7,2),(5,8,2),(6,9,2),(7,2,3),(8,3,3),(9,3,4),(10,6,4),(11,4,4),(12,3,5),(13,4,5),(14,5,5),(15,1,5),(16,2,5),(17,3,5);
/*!40000 ALTER TABLE `genre_show` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location` (
  `id_location` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(255) DEFAULT NULL,
  `id_scene` int(11) DEFAULT NULL,
  `seat_number` int(11) NOT NULL,
  `seat_row` int(11) NOT NULL,
  PRIMARY KEY (`id_location`),
  KEY `FK1ir8f6mhso2bn0ey6oh6ev42u` (`id_scene`),
  CONSTRAINT `FK1ir8f6mhso2bn0ey6oh6ev42u` FOREIGN KEY (`id_scene`) REFERENCES `scene` (`id_scene`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'1',1,1,1),(2,'1',1,2,1),(3,'1',1,3,1),(4,'1',1,1,2),(5,'1',1,2,2),(6,'1',1,3,2),(7,'1',1,1,3),(8,'1',1,2,3),(9,'1',1,3,3),(10,'1',2,1,1),(11,'1',2,2,1),(12,'1',2,3,1),(13,'1',2,1,2),(14,'1',2,2,2),(15,'1',2,3,2),(16,'1',3,1,1),(17,'1',3,1,2),(18,'1',3,1,3);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performance`
--

DROP TABLE IF EXISTS `performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `performance` (
  `id_performance` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `id_scene` int(11) DEFAULT NULL,
  `id_show` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_performance`),
  KEY `FKdpsgct0esirgn2cularve5nc9` (`id_scene`),
  KEY `FKi8eprhfc6t4fu7xcrnsbkec7r` (`id_show`),
  CONSTRAINT `FKdpsgct0esirgn2cularve5nc9` FOREIGN KEY (`id_scene`) REFERENCES `scene` (`id_scene`),
  CONSTRAINT `FKi8eprhfc6t4fu7xcrnsbkec7r` FOREIGN KEY (`id_show`) REFERENCES `show_theatre` (`id_show`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performance`
--

LOCK TABLES `performance` WRITE;
/*!40000 ALTER TABLE `performance` DISABLE KEYS */;
INSERT INTO `performance` VALUES (1,'2022-03-18',1,1),(2,'2022-03-18',2,2),(3,'2022-03-18',3,3),(4,'2022-03-19',1,4),(5,'2022-03-18',2,5),(6,'2022-03-22',2,1),(7,'2022-03-22',3,2),(8,'2022-03-22',1,4),(9,'2022-03-22',1,5);
/*!40000 ALTER TABLE `performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating` (
  `id_rating` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `id_performance` int(11) DEFAULT NULL,
  `id_visitor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rating`),
  KEY `FK4x6scy694tnj40s2b8hllmuvo` (`id_performance`),
  KEY `FKixx0p3fr63ljg98iimccocqw7` (`id_visitor`),
  CONSTRAINT `FK4x6scy694tnj40s2b8hllmuvo` FOREIGN KEY (`id_performance`) REFERENCES `performance` (`id_performance`),
  CONSTRAINT `FKixx0p3fr63ljg98iimccocqw7` FOREIGN KEY (`id_visitor`) REFERENCES `visitor` (`id_visitor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `id_show` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_role`),
  KEY `FK1jpsg5rtumjc4afaxh6537bdc` (`id_show`),
  CONSTRAINT `FK1jpsg5rtumjc4afaxh6537bdc` FOREIGN KEY (`id_show`) REFERENCES `show_theatre` (`id_show`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Hamlet',1),(2,'Cludius',1),(3,'Getrude',1),(4,'Ghost',1),(5,'Polonius',1),(6,'Laertes',1),(7,'Ophelia',1),(8,'Horatio',1),(9,'Marry Poppins',2),(10,'Bert',2),(11,'George',2),(12,'Wubbufred',2),(13,'Ellen',2),(14,'Brill-Cook',2),(15,'Jane',2),(16,'Michael',2),(17,'Atticus',3),(18,'Dill',3),(19,'Sheriff tate',3),(20,'Muadie',3),(21,'Dubose',3),(22,'Tom',3),(23,'Calpurnia',3),(24,'Judge Taylor',3),(25,'Macbeth',4),(26,'Lady Macbeth',4),(27,'Ross',4),(28,'Banquo',4),(29,'Duncan',4),(30,'Macduff',4),(31,'Malcom',4),(32,'Lennox',4),(33,'Ben Wade',5),(34,'Dan Evans',5),(35,'Charlie Prince',5);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scene`
--

DROP TABLE IF EXISTS `scene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scene` (
  `id_scene` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_scene`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scene`
--

LOCK TABLES `scene` WRITE;
/*!40000 ALTER TABLE `scene` DISABLE KEYS */;
INSERT INTO `scene` VALUES (1,'Big scene'),(2,'Small scene'),(3,'Scene in honor off...');
/*!40000 ALTER TABLE `scene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `show_theatre`
--

DROP TABLE IF EXISTS `show_theatre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `show_theatre` (
  `id_show` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `length` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `id_director` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_show`),
  KEY `FK6vsoixn968gd9aq96r8t20vlh` (`id_director`),
  CONSTRAINT `FK6vsoixn968gd9aq96r8t20vlh` FOREIGN KEY (`id_director`) REFERENCES `director` (`id_director`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `show_theatre`
--

LOCK TABLES `show_theatre` WRITE;
/*!40000 ALTER TABLE `show_theatre` DISABLE KEYS */;
INSERT INTO `show_theatre` VALUES (1,'Modern-day New York City adaptation of Shakespeare\'s immortal story about Hamlet\'s plight to avenge his father\'s murder.',120,'Hamlet',1),(2,'In turn of the century London, a magical nanny employs music and adventure to help two neglected children become closer to their father.',130,'Marry Poppins',2),(3,'Atticus Finch, a widowed lawyer in Depression-era Alabama, defends a black man against an undeserved rape charge, and his children against prejudice.',129,'To Kill a Mocking Bird',3),(4,'A Scottish lord becomes convinced by a trio of witches that he will become the next King of Scotland, and his ambitious wife supports him in his plans of seizing power.',105,'The Tradgedy of Macbeth',4),(5,'Broke small-time rancher Dan Evans is hired by the stagecoach line to put big-time captured outlaw leader Ben Wade on the 3:10 train to Yuma but Wade\'s gang tries to free him.',92,'3:10 to Yuma',5);
/*!40000 ALTER TABLE `show_theatre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL AUTO_INCREMENT,
  `date_payed` date DEFAULT NULL,
  `date_reserved` date DEFAULT NULL,
  `price` double NOT NULL,
  `id_location` int(11) DEFAULT NULL,
  `id_performance` int(11) DEFAULT NULL,
  `id_visitor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ticket`),
  KEY `FK7e8kq7ei0cfs80p8dknei570k` (`id_location`),
  KEY `FKs7wkeue3tlb7wqpe2ugl4vkxl` (`id_performance`),
  KEY `FK4cc7sr4k26mjabhrnjbgblb77` (`id_visitor`),
  CONSTRAINT `FK4cc7sr4k26mjabhrnjbgblb77` FOREIGN KEY (`id_visitor`) REFERENCES `visitor` (`id_visitor`),
  CONSTRAINT `FK7e8kq7ei0cfs80p8dknei570k` FOREIGN KEY (`id_location`) REFERENCES `location` (`id_location`),
  CONSTRAINT `FKs7wkeue3tlb7wqpe2ugl4vkxl` FOREIGN KEY (`id_performance`) REFERENCES `performance` (`id_performance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitor`
--

DROP TABLE IF EXISTS `visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visitor` (
  `id_visitor` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_visitor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitor`
--

LOCK TABLES `visitor` WRITE;
/*!40000 ALTER TABLE `visitor` DISABLE KEYS */;
INSERT INTO `visitor` VALUES (1,'milan@gmail.com','Milana','Jovic','milan','milan'),(2,'ana@gmail.com','Ana','Jovic','ana','ana');
/*!40000 ALTER TABLE `visitor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-19  0:28:54
